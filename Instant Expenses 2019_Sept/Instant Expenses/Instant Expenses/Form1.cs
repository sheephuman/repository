﻿using Microsoft.VisualBasic;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Windows.Forms;



namespace Instant_Expenses
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            dgv_WheelFix1.Columns[2].DefaultCellStyle.Format = "M月dd日";

            //ジェネリック型でイベントハンドラ型を定義

            //マウスホイールイベントの追加
            dateTimePicker1.MouseWheel
              += new MouseEventHandler(dateTimePicker1_MouseWheel);

            // dgV_WheelFix1.MouseWheel += new MouseEventHandler(dgW_WheelFix1_MouseWheel);
            //オーバーライドしたため無効に

            // listBox1.MouseWheel += new MouseEventHandler(listBox1_MouseWheel);


        }





        string current = Directory.GetCurrentDirectory();

        void ファイルリスト取得()
        {
            comboBox1.Items.Clear();

            string[] files = Directory.GetFiles(
    current + "\\kakeibo\\", "*", SearchOption.AllDirectories);


            foreach (string file in files)
            {

                comboBox1.Items.Add(Path.GetFileName(file));

                //  MessageBox.Show(Path.GetFileName(file));
            }


        }






        string LastLoad;

        private void Form1_Load(object sender, EventArgs e)
        {


            try
            {
                toolTip1.SetToolTip(CostBox, "ガイド表示は削除しなくても入力可能です　\n 数値を入力してEnter \n Shift + 下キーで履歴表示 　\n履歴はShift+Deleteで自由に削除できます");
                toolTip2.SetToolTip(dateTimePicker1, "マウスホイールで日付変更");
                toolTip3.SetToolTip(DetailBox, "自動でひらがな入力になります \n EnterかShiftキーでCost入力へ\n Shift + 下キーで履歴表示　\n履歴はShift+Deleteで自由に削除できます");


                dgv_WheelFix1.Columns[0].HeaderText = "支出";
                dgv_WheelFix1.Columns[1].HeaderText = "項目";
                dgv_WheelFix1.Columns[2].HeaderText = "日付";
                dgv_WheelFix1.Columns[3].HeaderText = "内訳";



                dgv_WheelFix1.Columns[2].DefaultCellStyle.Format = "M月dd日";


                DataGridViewColumn sortColumn = this.dgv_WheelFix1.Columns[2];

              
                EditBool = false;

                ////chartに反映させるためのデータテーブル
                //DataTable dt = new DataTable();
                //dgW_WheelFix1.DataSource = dt;


                //chart1.DataSource = dt;



                CostBox.Text = "ここに数値を入力";

                DetailBox.Text = "内訳";

                Itemリスト取得();

                ReWriteStripBtn.Enabled = false;

                ファイルリスト取得();

                listBox_wheelfix1.SelectedIndex = 0;

                DetailBox.Focus();

                dgv_WheelFix1.NotifyCurrentCellDirty(true);
                //  STF.Topradio.Checked = true;



                //listBox1.SelectedIndex = 0;


                //設定を読み込む
                LastLoad = Properties.Settings.Default.LastLoadFile;
                BottomAdd.Checked = Properties.Settings.Default.BottomAdd_CheckState;

                YearChangeState.Checked = Properties.Settings.Default.YearCangeChecked;
                Series_SUM_Chacker.Checked = Properties.Settings.Default.Series_SUMCheckedState;
                Cost_ClearedState.Checked = Properties.Settings.Default.Cost_Cleared;
                Size = Properties.Settings.Default.WindowsSize;


                // SplitContainer
                splitContainer2.Panel2Collapsed = Properties.Settings.Default.spl2_Colasp;
                splitContainer2.FixedPanel = Properties.Settings.Default.spl2_Fixed;


                              // splitContainer1.SplitterDistance = 90;
                splitContainer5.SplitterDistance = 92;
                splitContainer1.SplitterDistance = 158;
                splitContainer2.SplitterDistance = 412;

                if (LastLoad != "")
                {

                    dgv_WheelFix1.Rows.Clear();


                    CSV_to_DataGridView(LastLoad);
                    // Dt = GetDataTableFromCSV(LastLoad, false);
                    //最後に開いたファイルをDatatableに格納

                    // dgV_WheelFix1.DataSource = Dt;
                    //DataGridViewのデータソースにデータテーブルを指定


                    comboBox1.Text = Path.GetFileName(LastLoad);
                    Text = "Instant Expenses -" + comboBox1.Text;
                    // 自動でサイズを設定するのは、行や列を追加したり、セルに値を設定した後にする。

                    CSV_to_chart(comboBox1.Text);


                    TotalCostCalc();
                }



                if (Properties.Settings.Default.Side_Buttom_profile == 0)
                {
                    //ここから
                    splitContainer2.SplitterDistance = 213;
                    this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);

                    splitContainer5.SplitterDistance = 96;
                    splitContainer4.SplitterDistance = 455;
                   
                }
                else if (Properties.Settings.Default.Side_Buttom_profile == 1)
                {

                    splitContainer2.SplitterDistance = 213;
                    this.splitContainer4.Panel2.Controls.Add(this.splitContainer3);
                    splitContainer4.SplitterDistance = 412;


                    splitContainer5.SplitterDistance = 96;
                    splitContainer4.SplitterDistance = 455;
                  
                }

                CommmonColor = chart1.Series[0].Color = Properties.Settings.Default.CommonColor;
                //チャート用カラー設定変数の読み込み


                dateTimePicker1.Format = DateTimePickerFormat.Custom;
                dateTimePicker1.CustomFormat = "yyyy年M月d日";
                //M月d日　じゃないと上手くソート出来ない


                Historyリスト取得("\\Details.txt");
                Historyリスト取得("\\Costs.txt");


            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);

            }

        }





        private void ExitStripButton_Click(object sender, EventArgs e)
        {


            Application.Exit();
        }

        //private void SetTimeButton_Click(object sender, EventArgs e)
        //{
        //    DateTime DT = DateTime.Now;
        //    dateTimePicker1.Text = DT.ToString();
        //}
        //}

        private void SaveStripButton_Click(object sender, EventArgs e)
        {
            dgv_WheelFix1.EndEdit();


            saveFileDig_Method();

            ファイルリスト取得();

            string FileNameSt = Path.GetFileName(sfd.FileName);

            Properties.Settings.Default.LastLoadFile = sfd.FileName;




            //アプリのタイトルとファイル名




            //上書き保存ボタンのenable制御
            //if (dgV_WheelFix1.Rows[0].Cells[0].Value != null)
            //{
            ReWriteStripBtn.Enabled = false;

            NewData = false;


            EditBool = false; //未編集に


            comboBox1.Text = FileNameSt;

        }












        //正規表現
        Regex re = new Regex("[０-９]");

        //正規表現の検索
        static string myReplacer(Match m)
        {
            return Strings.StrConv(m.Value, VbStrConv.Narrow, 0);

            //using Microsoft.visualBasic;

        }

        bool EditBool = false;  //編集済み判定用　false: 未編集
        bool DetailFocus = false; //内訳ボックスのフォーカス判定

        private void CostBox_KeyDown(object sender, KeyEventArgs e)
        {




            if (DetailFocus == false)
            {


                //DetailBox.Text = "";
                DetailFocus = true;

            }




            if (e.KeyCode == Keys.Enter && CostBox.Text != "" && CostBox.Text != "ここに数値を入力")
            {


                //数字以外の文字列を置換する
                CostBox.Text = re.Replace(CostBox.Text, myReplacer);


                dgv_WheelFix1.MultiSelect = false;



                //編集済み、もしくは新規データの場合
                if (EditBool == true || NewData == true)
                {
                    ReWriteStripBtn.Enabled = false;
                }


                //int DataAbouveRow = dgW_WheelFix1.Rows.Count;
                int EndData = dgv_WheelFix1.Rows.Count - 1;



                if (CostBox.Text != "" && dgv_WheelFix1.Rows[EndData].Cells[0].Value == null)
                {

                    //再描画（ちらつき）の抑止
                    CostBox.BeginUpdate();


                    DetailBox.BeginUpdate();


                    #region 末尾から追加する処理
                    if (BottomAdd.Checked == true)
                    {




                        
                        dgv_WheelFix1.Rows.Add(1);


                        dgv_WheelFix1.Rows[EndData].Cells[0].Value = CostBox.Text;



                        dgv_WheelFix1.Rows[EndData].Cells[1].Value = listBox_wheelfix1.SelectedItem;

                        // dgV_WheelFix1.Rows[EndData].Cells[2].Value = dateTimePicker1.Text.Substring(5, dateTimePicker1.Text.Length - 5);

                        dgv_WheelFix1.Rows[EndData].Cells[2].Value = dateTimePicker1.Value.ToString("MM月dd日");

                        //DateTime.Parse(dgV_WheelFix1.Rows[EndData].Cells[2].Value.ToString()).ToString("yyyy年MM月dd日");


                        dgv_WheelFix1.Rows[EndData].Cells[3].Value = DetailBox.Text;







                        dgv_WheelFix1.FirstDisplayedScrollingRowIndex = dgv_WheelFix1.Rows.Count - 1;


                        dgv_WheelFix1.Rows[dgv_WheelFix1.Rows.Count - 2].Selected = true;



                        if (EditBool == false)
                        {
                            EditBool = true; //編集済みに
                            Text += "*";
                        }
                    }




                    //  dgW_WheelFix1.UpdateCellValue(2,EndData);

                    //常に直近の入力データをフォーカス



                    #endregion

                    #region 先頭から追加する処理
                    else if (BottomAdd.Checked == false)
                    {
                        dgv_WheelFix1.Rows.Add(1);



                        //列数Forループ
                        for (int i = dgv_WheelFix1.RowCount - 1; i >= 1; i--)
                        {
                            //行数forループ
                            for (int j = 0; j <= dgv_WheelFix1.ColumnCount - 1; j++)
                            {
                                if (i == 1) //行数が1の時
                                {
                                    dgv_WheelFix1.Rows[i].Cells[j].Value = dgv_WheelFix1.Rows[i - 1].Cells[j].Value;
                                }

                                else if (i > 1)
                                {
                                    //一列ずつズラして空きを作る
                                    dgv_WheelFix1.Rows[i - 1].Cells[j].Value = dgv_WheelFix1.Rows[i - 2].Cells[j].Value;
                                }
                            }

                        }


                        if (DetailBox.Text == "内訳") DetailBox.Text = "";



                        dgv_WheelFix1.Rows[0].Cells[0].Value = CostBox.Text;



                        dgv_WheelFix1.Rows[0].Cells[1].Value = listBox_wheelfix1.Text;

                        //   dgv_WheelFix1.Rows[0].Cells[2].Value = dateTimePicker1.Text.Substring(5, dateTimePicker1.Text.Length - 5);

                        dgv_WheelFix1.Rows[0].Cells[2].Value = dateTimePicker1.Value.Date.ToString("MM月dd日"); ;

                        dgv_WheelFix1.Rows[0].Cells[3].Value = DetailBox.Text;



                        dgv_WheelFix1.FirstDisplayedScrollingRowIndex = 0;
                        //先頭行を表示

                        dgv_WheelFix1.Rows[0].Selected = true;


                    }


                    if (EditBool == false)
                    {
                        EditBool = true; //編集済みに
                        Text += "*";
                    }
                }
                #endregion





                if (!DetailBox.Items.Contains(DetailBox.Text) && DetailBox.Text != "")
                {

                    DetailBox.Items.Add(DetailBox.Text);
                    //入力したものを履歴として格納

                }

                if (!CostBox.Items.Contains(CostBox.Text) && CostBox.Text != "")
                {
                    CostBox.Items.Add(CostBox.Text);
                    //入力したものを履歴として格納

                }



                if (Cost_ClearedState.Checked == true)
                {
                    CostBox.Text = "";
                    DetailBox.Text = "";
                }


                TotalCostCalc();


            }

            //Shift+↓が押されたとき
            else if (e.Shift == true && e.KeyCode == Keys.Down)
            {
                CostBox.Text = "";
                CostBox.DroppedDown = true;


                CostBox.Text = "";
            }

            //Deleteキーが押され、コストボックスにItemがある場合
            else if (e.KeyCode == Keys.Delete && CostBox.Items.Count > 1)
            {

                int Select;

                Select = CostBox.SelectedIndex;

                CostBox.Items.Remove(CostBox.SelectedItem);




                if (CostBox.SelectedIndex < CostBox.Items.Count)
                {
                    if (CostBox.SelectedIndex == -1)
                    {
                        CostBox.SelectedIndex = 0;

                    }
                    if (Select > 0)
                    {

                        CostBox.SelectedIndex = Select - 1;
                    }
                }


                CostBox.Text = "";
            }


            //再描画させる
            CostBox.EndUpdate();
            DetailBox.EndUpdate();





        }


        //ComboBox
        private void CostBox_KeyPress(object sender, KeyPressEventArgs e)
        {


            // 数字(0-9)は入力可

            //   String str = CostBox.Text;



            if (char.IsDigit(e.KeyChar) || e.KeyChar == '\b')
            {

                //数字が文字と混在したときの対策
                if (CostBox.Text == "ここに数値を入力")
                    CostBox.Text = "";

                e.Handled = false;
                return;






            }

            // 上記条件は入力不可
            e.Handled = true;






            // System.Threading.Thread.Sleep(1000);

        }

        public class NumericTextBox : TextBox
        {

            const int ES_NUMBER = 0x2000;
            protected override CreateParams CreateParams
            {
                [SecurityPermission(SecurityAction.Demand,
                    Flags = SecurityPermissionFlag.UnmanagedCode)]


                get
                {
                    CreateParams parms = base.CreateParams;
                    parms.Style |= ES_NUMBER;
                    return parms;
                }

            }
        }


        public void OpenF_Method()
        {
            ofd.InitialDirectory = Directory.GetCurrentDirectory() + "\\kakeibo\\";

            //新規データでYesが押された場合
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //OKボタンがクリックされたとき
                //選択されたファイル名を表示する

                // ofd.FileName = dateTimePicker1.Text;
                CommonPathString = ofd.FileName;
              

                ofd.Filter =
        "CSVファイル(*.csv)|*.csv|すべてのファイル(*.*)|*.*";
                ofd.FilterIndex = 1;

                ofd.Title = "開くファイルを選択してください";



                ofd.RestoreDirectory = true;

                CSV_to_DataGridView(ofd.FileName);

                TotalCostCalc();


                //  graph_chart_display();

                try
                {
                    dateTimePicker1.Text = ofd.SafeFileName.Replace(".csv", "");
                }

                catch (FormatException)
                {
                    // MessageBox.Show(FE.Message);
                }

                ReWriteStripBtn.Enabled = false;



            }
        }


        String CommonPathString;
        OpenFileDialog ofd = new OpenFileDialog();

        private void OpenFileButton_Click(object sender, EventArgs e)
        {


            //編集済みか
            if (EditBool)
            {
                DialogResult result = MessageBox.Show("ファイルが保存されていません。保存しますか？",
                "質問",
                 MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2);



                //「はい」が選択された時
                if (result == DialogResult.Yes)
                {


                    saveFileDig_Method();


                    EditBool = false; //未編集


                }

                else if (result == DialogResult.No)
                {
                    //「いいえ」が選択された時

                    OpenF_Method();


                }





                TotalCostCalc();




            }
            else
            {
                OpenF_Method();


            }




            if (ofd.SafeFileName != "")
            {
                comboBox1.Text = ofd.SafeFileName;
                Text = "Instant Expenses -" + ofd.SafeFileName;
            }



        }



        private void SideButton_Click(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.Side_Buttom_profile = 1;

                this.splitContainer4.Panel2.Controls.Add(this.splitContainer3);
                splitContainer2.Panel2Collapsed = true;

                splitContainer2.Panel2Collapsed = true;
                splitContainer2.FixedPanel = FixedPanel.Panel2;

                splitContainer4.Panel2Collapsed = false;
                splitContainer4.FixedPanel = FixedPanel.Panel1;



                //      label2.Text = Height.ToString(); ;
                Height = 411;
                Width = 1078;

                splitContainer1.SplitterDistance = 162;
                splitContainer5.SplitterDistance = 96;
                splitContainer4.SplitterDistance = 455;
            }

            catch (ArgumentException)
            { }
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            //label2.Text = Height.ToString();
            //label3.Text = Width.ToString();


        }




        private void BottomButton_Click(object sender, EventArgs e)
        {

            Properties.Settings.Default.Side_Buttom_profile = 0;

            splitContainer4.Panel2Collapsed = true;
            splitContainer4.FixedPanel = FixedPanel.Panel1;


            splitContainer2.Panel2Collapsed = false;
            splitContainer2.FixedPanel = FixedPanel.None;

            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
          

            Height = 718;
            Width = 626;
            // splitContainer2.SplitterDistance = 252;
          //  splitContainer5.SplitterDistance = 300;
            
           // splitContainer4.SplitterDistance = 455;
          
            splitContainer1.SplitterDistance = 162;
            splitContainer2.SplitterDistance = 213;
            splitContainer5.SplitterDistance = 96;
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            //if (Width > 1000)
            //{
            //    chart1.Dock = DockStyle.None;
            //    label2.Text = Width.ToString();
            //    label3.Text = chart1.Width.ToString();
            //    chart1.Width = Width - 522; // 疑似的にサイズを追随させる。

            //}

            //else if(chart1.Width < 595)
            //{
            //    label3.Text = chart1.Width.ToString();
            //    chart1.Width = 595;
            
            //}
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {

            CSV_to_chart(comboBox1.Text);
        }



        Color CommmonColor;
        
        private void Chart_ColorChangeBtn_Click(object sender, EventArgs e)
        {
               //グラフ色変更
                try
                {
                    Graphics g = this.CreateGraphics();
                    ColorDialog cd = new ColorDialog();
                    cd.Color = Color.FromKnownColor(KnownColor.DarkTurquoise);
                    cd.FullOpen = true;
                    cd.CustomColors = new int[] { ColorTranslator.ToWin32(Color.OrangeRed),
                ColorTranslator.ToWin32(Color.LawnGreen),
                ColorTranslator.ToWin32(Color.LightYellow),
                ColorTranslator.ToWin32(Color.AliceBlue),
                0x00880000,0x00008800,0x00000088,0x00ff8800 };
                    if (cd.ShowDialog() == DialogResult.OK)
                    {
                        //  SolidBrush brush = new SolidBrush(cd.Color);

                        CommmonColor = cd.Color;
                        chart1.Series[0].Color = cd.Color;
                    }

                }

                catch (ArgumentOutOfRangeException)
                { }
            }

        private void dgV_WheelFix1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            //新規データではない場合
            if (NewData == false)
            {
                ReWriteStripBtn.Enabled = true;
            }

            CostBox.Focus();


            //未編集だった場合
            if (EditBool == false)
            {
                EditBool = true;

                Text += "*";

            }
        }


        //クリックしたときのDataGridView内のマウス位置情報
        DataGridView.HitTestInfo hti;


        private void dgV_WheelFix1_MouseDown(object sender, MouseEventArgs e)
        {
            hti = ((DataGridView)sender).HitTest(e.X, e.Y);

        }

        private void dgW_WheelFix1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            TotalCostCalc();
            //graph_chart_display();
        }

      


            #region  ファイルソート時にDateTime型に変換し、日付をソートできるようにする。
            private void dgV_WheelFix1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
            {


            if (e.ColumnIndex == 2)
            {

                for (int i = dgv_WheelFix1.Rows.Count - 1; i >= 1; i--)
                {

                    if (dgv_WheelFix1.Rows[i].Cells[2].Value != null)
                    {
                        dgv_WheelFix1.Rows[i].Cells[2].Value = DateTime.Parse(dgv_WheelFix1.Rows[i].Cells[2].Value.ToString()).ToString("MM月dd日");
                    }
                }
                //MessageBox.Show(dgV_WheelFix1.Rows[e.RowIndex].Cells[2].Value.ToString());

            }
            if(e.ColumnIndex == 0)
            {

              

                //for (int i = dgv_WheelFix1.Rows.Count - 1; i >= 1; i--)
                //{

                //    if (dgv_WheelFix1.Rows[i].Cells[0].Value != null)
                //    {
                //        dgv_WheelFix1.Rows[i].Cells[0].Value = int.Parse(dgv_WheelFix1.Rows[i].Cells[0].Value.ToString());
                //    }
                //}


                //DataGridViewColumn sortColumn = dgv_WheelFix1.CurrentCell.OwningColumn;

                ////並び替えの方向（昇順か降順か）を決める
                //ListSortDirection sortDirection = ListSortDirection.Ascending;


                //if (dgv_WheelFix1.SortedColumn != null &&
                //    dgv_WheelFix1.SortedColumn.Equals(sortColumn))
                //{
                //    sortDirection =
                //        dgv_WheelFix1.SortOrder == SortOrder.Ascending ?
                //        ListSortDirection.Descending : ListSortDirection.Ascending;
                //}




                ////並び替えを行う
                //dgv_WheelFix1.Sort(sortColumn, sortDirection);


                //for (int i = dgv_WheelFix1.Rows.Count - 1; i >= 1; i--)
                //{

                //    if (dgv_WheelFix1.Rows[i].Cells[0].Value != null)
                //    {
                //        dgv_WheelFix1.Rows[i].Cells[0].Value = int.Parse(dgv_WheelFix1.Rows[i].Cells[0].Value.ToString());
                //    }
                //}

            }
        }
        #endregion

       
        

        private void hTML出力ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string current = Directory.GetCurrentDirectory();

            string path = current + "\\HTML Output\\";

            SaveHtml(path+comboBox1.Text.Replace(".csv",".HTML") );

            //for (int i = 0;  i <= dgV_WheelFix1.RowCount-1 ;i++)
            //{
            //    for(int j = 0;  j <= dgV_WheelFix1.ColumnCount-1 ; j++)
            //    {
            //       // SaveHtml(dgV_WheelFix1.Rows[i].Cells[j].Value.ToString());


            //    }

            //}
            MessageBox.Show("保存に成功しました。");
        }

        private void splitContainer5_Panel1_Paint(object sender, PaintEventArgs e)
        {
                 // label1.Text =  splitContainer5.SplitterDistance.ToString();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
         //   label1.Text = splitContainer1.SplitterDistance.ToString();
        }

        private void splitContainer1_Paint(object sender, PaintEventArgs e)
        {
          //  label1.Text = splitContainer1.SplitterDistance.ToString();
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {
            // label1.Text = splitContainer1.SplitterDistance.ToString();
          //  label1.Text = splitContainer1.SplitterDistance.ToString();
        }

        private void splitContainer4_Panel1_Paint(object sender, PaintEventArgs e)
        {
          //  label1.Text = splitContainer1.SplitterDistance.ToString();
            
            //label1.Text = splitContainer4.SplitterDistance.ToString();
        }

        private void splitContainer2_Panel1_Paint(object sender, PaintEventArgs e)
        {
         //   label1.Text = splitContainer1.SplitterDistance.ToString();
        }

        private void Series_SUM_Chacker_CheckedChanged(object sender, EventArgs e)
        {
            CSV_to_chart(comboBox1.Text);
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {
           // label1.Text = splitContainer1.SplitterDistance.ToString();
        }

        private void dgv_WheelFix1_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {

            if(e.Column.Index == 0)
            { 
            int str1 = int.Parse(e.CellValue1.ToString());

            int str2 = int.Parse(e.CellValue2.ToString());
          

            //結果を代入
            e.SortResult = str1 - str2;
            //処理したことを知らせる
            e.Handled = true;
            }
        }


       
    }
    
}


