﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Instant_Expenses
{
    public partial class Form1 : Form
    {





        //マウスホイールイベントがオーバーライドされているため無効に
        //private void dgW_WheelFix1_MouseWheel(object sender, MouseEventArgs e)
        //{

        //    int wheel = e.Delta / 120;

        //    int CurrentRow = dgV_WheelFix1.CurrentCell.RowIndex - wheel;


        //    int CurrentCell = dgV_WheelFix1.CurrentCell.ColumnIndex;

        //    dgV_WheelFix1.MultiSelect = false;

        //    try
        //    {



        //        dgV_WheelFix1.Rows[CurrentRow].Cells[CurrentCell].Selected = true;




        //        if (12 < CurrentRow)
        //        {
        //            dgW_WheelFix1.Rows[dgW_WheelFix1.RowCount - 1].Frozen = false;
        //    dgW_WheelFix1.Rows[11].Frozen = false;


        //    dgW_WheelFix1.FirstDisplayedScrollingColumnIndex = 0;
        //}




        //    catch (Exception ex)
        //    {
        //        //   MessageBox.Show(ex.Message, dgW_WheelFix1.RowCount.ToString());
        //        Console.WriteLine(ex);
        //    }




        //}



            //日付用変数
        DateTime DT = new DateTime();
        private void dateTimePicker1_MouseWheel(object sender, MouseEventArgs e)
        {

            int DTHeight = dateTimePicker1.Bounds.Height;
            Point PickerRectLocation = dateTimePicker1.PointToClient(dateTimePicker1.Bounds.Location);
            //datetimePickerのクライアント座標を、datetimePicker位置座標を元に出す



            Point WheelPoint = new Point(e.X, e.Y);

            // マウス座標をクライアント座標系へ変換
            Point mouseClientPos = dateTimePicker1.PointToClient(WheelPoint);

         
            //Rectangle YearRect;
            Rectangle YearRect = new Rectangle(PickerRectLocation.X -dateTimePicker1.Location.X, PickerRectLocation.Y - dateTimePicker1.Location.Y , 23, DTHeight);

            Rectangle MonthRect = new Rectangle(PickerRectLocation.X - dateTimePicker1.Location.X + 33, PickerRectLocation.Y - dateTimePicker1.Location.Y, 23, DTHeight);

            Rectangle DayRect = new Rectangle(PickerRectLocation.X - dateTimePicker1.Location.X + 57, PickerRectLocation.Y - dateTimePicker1.Location.Y + 3, 23, DTHeight);

            
           // if(DateTime.TryParseExact())

            if (YearRect.Contains(mouseClientPos) == true)
            {
                if (YearChangeState.Checked == false)
                {
                    // スクロール量（方向）の表示
                    DT = dateTimePicker1.Value.AddYears(e.Delta / 120);
                    dateTimePicker1.Text = DT.ToShortDateString();

                }

            }
            else if (MonthRect.Contains(mouseClientPos) == true)
            {

                DT = dateTimePicker1.Value.AddMonths(e.Delta / 120);
                dateTimePicker1.Text = DT.ToShortDateString();


            }
            else if (DayRect.Contains(mouseClientPos) == true)
            {

                DT = dateTimePicker1.Value.AddDays(e.Delta / 120);
                dateTimePicker1.Text = DT.ToLongDateString();


            }
        }
    }
}
