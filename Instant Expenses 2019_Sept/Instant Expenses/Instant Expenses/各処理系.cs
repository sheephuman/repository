﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.IO;
using System.Windows.Forms;
using Microsoft.VisualBasic.FileIO;

namespace Instant_Expenses
{
    partial class Form1
    {

        #region 合計値集計
        public void TotalCostCalc()
        {
            try
            {
                //ジェネリック
               var TotalCalc = new List<int>();

                for (int i = 0; i < dgv_WheelFix1.Rows.Count; i++)
                {
                    //if (i == 0) { label3.Text = (Convert.ToDecimal(dgW_WheelFix1.Rows[i].Cells[0].Value)).ToString(); }

                    if (i >= 0)
                    {
                        //if (i + 1 == dgW_WheelFix1.Rows.Count) break;
                        if (dgv_WheelFix1.Rows[i].Cells[0].Value != (object)"")
                        {
                            TotalCalc.Add(Convert.ToInt32(dgv_WheelFix1.Rows[i].Cells[0].Value));
                        }
                    }

                }
                label3.Text = TotalCalc.Sum().ToString("C");
                //"C"は通貨で使用される書式



            }

            catch (FormatException)
            { }



        }
        #endregion

        #region //Chart関連の処理

        private void CSV_to_DataGridView(string FileName)
        {
            try
            {
                dgv_WheelFix1.Rows.Clear();


                var parser = new TextFieldParser(FileName, Encoding.GetEncoding("Shift_JIS"));
                parser.TextFieldType = FieldType.Delimited;

                parser.SetDelimiters(","); // 区切り文字はコンマ


                while (!parser.EndOfData)
                {

                    string[] row = parser.ReadFields(); // 1行読み込み
                                                        // 読み込んだデータ(1行をDataGridViewに表示する)

                    dgv_WheelFix1.Rows.Add(row);
                }




                //if (dgW_WheelFix1.RowCount > 12)
                //{

                //    dgW_WheelFix1.Rows[11].Frozen = true;

                //    dgW_WheelFix1.Rows[dgW_WheelFix1.RowCount-1].Frozen = true;
                //}

                parser.Close();
            }

            catch (Exception ex)
            { Console.WriteLine(ex.Message); }

        }




        private void CSV_to_chart(string ReadFile)
        {



            chart1.Series.Clear();
            chart1.Series.Add("合計");







            chart1.Series[0].Color = CommmonColor;



            try
            {
                string current = Directory.GetCurrentDirectory();

                // Full path to the data source file

                string file = ReadFile;
                string path = current + "\\kakeibo\\";

                // Create a connection string.
                string ConStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                path + ";Extended Properties=\"Text;HDR=No;FMT=Delimited\"";
                OleDbConnection myConnection = new OleDbConnection(ConStr);



                string mySelectQuery = "";

                #region 試行錯誤
                // Create a database command on the connection using query
                //   string mySelectQuery = "Select F2, SUM(F1) AS F1S from" +"\\" + file + "GROUP BY F2 ORDER BY F2";

                //string mySelectQuery = "SELECT F2 FROM \\sample.csv GROUP BY F2 ORDER BY F2";


                //"D:\code\SheepMan Account Book\SheepMan Account Book\bin\Debug\kakeibo\2017年11月17日.csv"
                //string mySelectQuery = "SELECT F2,SUM(F1) AS F1S FROM"+ file+ "GROUP BY F2 ORDER BY F2";
                //  string mySelectQuery = "SELECT MIN(F3),SUM(F1) FROM  " +"\\" +file+ " GROUP BY F3";

                //  string mySelectQuery = "SELECT F3,Sum(F1) FROM " + "\\" + file +" Where F3 IN(Select F3 From " +"\\"+file+ " Group by F3 Order by f3)";
                // 集計関数の一部として指定された式 'F3' を含んでいないクエリを実行しようとしました。

                //string mySelectQuery = "SELECT F2, SUM(F1) AS F1S" + "WHERE F2 LIKE '%水道代%' GROUP BY F2 ORDER BY F2 FROM" +"\\" + file;

                #endregion


                //系列ごとに集計する場合
                if (Series_SUM_Chacker.Checked == true)
                {
                    mySelectQuery = "SELECT F2,Sum(F1) FROM " + "\\" + "[" + file + "]" + " Group by F2";
                              //+、-スペース入りファイル名も使えるようにする


                }




                else
                {
                    // string[] seriesArray = Dt.AsEnumerable().Select(r => r.Field<string>("F2")).Distinct().ToArray();

                    //for (int k = 0; k < seriesArray.Length; k++)
                    //{
                    //    string SeriesName = (from DataRow row in dt.Rows where (string)row["F2"] == seriesArray[k]).ToString();
                    //}


                    mySelectQuery = "SELECT F3,Sum(F1) FROM " + "\\" +"[" + file+"]" + " Group by f3 Order by Format(F3,'MMDD')";
                        //+、-スペース入りファイル名も使えるようにする
                    
                    
                    
                    
                    //Formatはsort用
                    //mySelectQuery = "SELECT *,Sum(F1) FROM " + "\\" + file + " WHERE F2 = (SELECT F2 FROM " + "\\" + file + ")";

                   // mySelectQuery = "SELECT F3,SUM(F1) FROM(SELECT F3 FROM " + "\\" + file + ")" ; 

   //                    SELECT SUM(F1) AS F3
   //   FROM(SELECT COUNT(*) AS F3 FROM F2 GROUP BY F3) AS F2; 


                    //SELECT F3 FROM \\file
                    //  WHERE ProductID =
                    //  (SELECT ProductID FROM Production.Product
                    //   WHERE Name = 'Chain')

                    //    string mySelectQuery = "SELECT F3,Sum(F1) FROM " + " Where F2 IN (SELECT SUM(F1) FROM) "+"\\"+file + " Group by f3 Order by Format(F3,'MMDD')";


                    //　系列F3からF1を集計　F3（日付）からグルーピングとソート

                    //Group by とOrder by を併用しないと通らなかった　集計関数の一部として指定された式 'F3' を含んでいないクエリを実行しようとしました。


                    // string mySeriesData = "SELECT F4 FROM " + "\\" + file + " Group by f4 Order by F4";




                }



                chart1.Series[0].Label = "#VAL{C}";
                //Chartに価格ラベルを表示する



                OleDbCommand myCommand = new OleDbCommand(mySelectQuery, myConnection);
                // Open the connection and create the reader
                myCommand.Connection.Open();
                OleDbDataReader myReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);


                chart1.ChartAreas[0].InnerPlotPosition.Width = 90;
                chart1.ChartAreas[0].InnerPlotPosition.Height = 90;
                chart1.ChartAreas[0].InnerPlotPosition.X = 8;
                chart1.ChartAreas[0].InnerPlotPosition.Y = 0;


                chart1.ChartAreas[0].AxisX.Interval = 1;
                //軸間隔 1で全部表示


                chart1.ChartAreas[0].IsSameFontSizeForAllAxes = true;


                chart1.Series[0].Points.DataBindXY(myReader, "0", myReader, "1");

                myReader.Close();
                myConnection.Close();


            }
            catch (OleDbException OleEx)
            { //Console.WriteLine(OleEx.ToString());

                MessageBox.Show(OleEx.Message);
            }
        }


        #endregion



        #region File取得・Writing関連の処理

        void Itemリスト取得()
        {
           var sr = new StreamReader(
        current + "\\sort.txt", Encoding.GetEncoding("Shift_JIS"));
            while (sr.Peek() >= 0)
            {
                // ファイルを 1 行ずつ読み込む
                string stBuffer = sr.ReadLine();
                // 読み込んだものを追加で格納する
                listBox_wheelfix1.Items.Add(stBuffer);



            }
            sr.Close();
        }



        void Historyリスト取得(string FileName)
        {
            var De_list = new List<string>();         //空のListを作成する

            var Co_list = new List<string>();


            using (System.IO.StreamReader file = new System.IO.StreamReader(current + FileName, Encoding.GetEncoding("shift_JIS")))
            {
                string line = "";

                if (FileName == "\\Details.txt")
                {
                    // test.txtを1行ずつ読み込んでいき、末端(何もない行)までwhile文で繰り返す
                    while ((line = file.ReadLine()) != null)
                    {
                        De_list.Add(line);
                    }

                    foreach (var item in De_list)
                    {

                        //DetailBoxになければ追加する
                        if (!DetailBox.Items.Contains(item) && item != "")
                        {
                            DetailBox.Items.Add(item);


                        }

                    }
                }


                else if (FileName == "\\Costs.txt")
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        Co_list.Add(line);
                    }
                    foreach (var item in Co_list)
                    {

                        //DetailBoxになければ追加する
                        if (!CostBox.Items.Contains(item) && item != "")
                        {
                            CostBox.Items.Add(item);


                        }

                    }

                }


            }

        }



        //内訳リスト書き込み処理
        public void HistoyItms_Write(string FileName)
        {
            string filePath = current + FileName;
            // string Writetext = "";

             var de_Write = new List<string>();

             var Co_Write = new List<string>();




            using (StreamWriter sw = new StreamWriter(filePath, false, Encoding.GetEncoding("Shift_JIS")))
            {
                if (FileName == "\\Details.txt")
                {

                    for (int i = 0; i < DetailBox.Items.Count; i++)
                    {
                        // Writetext += DetailBox.Items[i].ToString() + sw.NewLine;

                        de_Write.Add(DetailBox.Items[i].ToString());
                    }
                    //De_Writeに格納する

                    foreach (var item in de_Write)
                    {

                        //// de_Write内になければ追加する
                        //if (!de_Write.Contains(item))
                        //{
                        sw.Write(item + sw.NewLine);


                    }

                }


                else if (FileName == "\\Costs.txt")
                {
                    for (int i = 0; i < CostBox.Items.Count; i++)
                    {

                        Co_Write.Add(CostBox.Items[i].ToString());


                    }

                    //Co_Writeを保存する
                    foreach (var item in Co_Write)
                    {

                        //// de_Write内になければ追加する
                        //if (!de_Write.Contains(item))
                        //{
                        sw.Write(item + sw.NewLine);


                    }

                    sw.Close();
                }
            }
            #endregion

        }


        #region ファイルのIO
        SaveFileDialog sfd = new SaveFileDialog();
        public void WritingFileMethod(string FileName)
        {

            String strCsvData = string.Empty;
            //SaveFileDialogクラスのインスタンスを作成




            try
            {

                if (sfd.FileName != "" || Properties.Settings.Default.LastLoadFile != "")
                {
                    using (var writer = new StreamWriter(FileName, false, Encoding.GetEncoding("shift_jis")))
                    {

                        int rowCount = dgv_WheelFix1.Rows.Count;



                        // ユーザによる行追加が許可されている場合は、最後に新規入力用の
                        // 1行分を差し引く
                        if (dgv_WheelFix1.AllowUserToAddRows == true)
                        {
                            rowCount = rowCount - 1;
                        }




                        // 行
                        for (int i = 0; i < rowCount; i++)
                        {
                            // リストの初期化
                           var strList = new List<String>();

                            // 列
                            for (int j = 0; j < dgv_WheelFix1.Columns.Count; j++)
                            {
                                if (dgv_WheelFix1[j, i].Value != null)
                                {
                                    strList.Add(dgv_WheelFix1[j, i].Value.ToString());
                                }
                                else if (dgv_WheelFix1[j, i].Value == null)
                                {
                                    dgv_WheelFix1[j, i].Value = "";
                                    strList.Add(dgv_WheelFix1[j, i].Value.ToString());
                                }
                            }
                            // String[] strArray = strList.ToArray();  // 配列へ変換

                            // CSV 形式に変換
                            strCsvData = String.Join(",", strList);


                            writer.WriteLine(strCsvData);

                            Text = Text.Replace("*", "");


                        }
                        writer.Close();
                    }

                }



            }
            //catch (ArgumentException AE)
            //{ MessageBox.Show(AE.Message); }

            catch (System.FormatException FE)
            { MessageBox.Show(FE.Message); }

        }



         //保存時のダイアログ呼び出し
        public void saveFileDig_Method()
        {
            //ダイアログを表示する
            sfd.DefaultExt = "*.csv";


           // sfd.FileName = "";
            sfd.InitialDirectory = Directory.GetCurrentDirectory() + "\\kakeibo\\";

            sfd.Filter = "CSVファイル|*.csv|全てのファイル|*.*";
            if (checkBox1.Checked == true)
            {
                dateTimePicker1.Format = DateTimePickerFormat.Custom;
                dateTimePicker1.CustomFormat = "yyyy年M月";
                sfd.FileName = dateTimePicker1.Text;  //.Text + ".csv";

               
                dateTimePicker1.Format = DateTimePickerFormat.Long;
                //カスタムフォーマットのリセット



            }

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //ファイル書き込み
                WritingFileMethod(sfd.FileName);



                //チャート読み込み
                CSV_to_chart(Path.GetFileName(sfd.FileName));

                Text = "Instant Expenses -" + Path.GetFileName(sfd.FileName);
            }


        }
        #endregion


        public void SaveHtml(string fp)
        {
            // HTMLファイルオープン
            StreamWriter sw =
                new StreamWriter(fp, false,
                System.Text.Encoding.GetEncoding("SHIFT-JIS"));

            // HTMLファイル書込(ヘッダー部)
            sw.WriteLine("<html>");
            sw.WriteLine("<head>");
            sw.WriteLine("<meta http-equiv=\"Content-Language\" ");
            sw.WriteLine("content=\"ja\">");
            sw.WriteLine("<meta http-equiv=\"Content-Type\" ");
            sw.WriteLine("content=\"text/html;charset=shift_jis\">");
            sw.WriteLine("<title>DataGridView(C#.NET)</title>");
            sw.WriteLine("</head>");
            sw.WriteLine("<body>");
            sw.WriteLine("<table border=\"1\" cellspacing=\"0\" ");
            sw.WriteLine("style=\"table-layout:fixed;\">");

            for (int r = 0; r <= dgv_WheelFix1.Rows.Count - 1; r++)
            {
                // HTMLファイル書込(各行)
                sw.Write("<tr>");
                for (int c = 0; c <= dgv_WheelFix1.Columns.Count - 1; c++)
                {
                    // DataGridViewのセルのデータ取得
                    String dt = "";
                    if (dgv_WheelFix1.Rows[r].Cells[c].Value != null)
                    {
                        dt = dgv_WheelFix1.Rows[r].Cells[c].Value.
                            ToString();
                    }
                    // HTMLエンコード
                    if (dt == "") dt = " ";
                    dt = dt.Replace("&", "&amp;");
                    dt = dt.Replace("<", "&lt;");
                    dt = dt.Replace(">", "&gt;");
                    dt = dt.Replace(" ", "&nbsp;");
                    // HTMLファイル書込(各セル)
                    sw.Write("<td>");
                    sw.Write(dt);
                    sw.Write("</td>");
                }
                // HTMLファイル書込(各行)
                sw.Write("</tr>");
                sw.Write("\n");
            }

            // HTMLファイル書込(フッター部)
            sw.WriteLine("</table>");
            sw.WriteLine("</body>");
            sw.WriteLine("</html>");

            // HTMLファイルクローズ
            sw.Close();
        }

    }
}

