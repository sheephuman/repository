﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Instant_Expenses
{
    public partial class ListBox_wheelfix : ListBox
    {
        public ListBox_wheelfix()
        {
            InitializeComponent();
        }
        /// <summary>
        ///       ///
        /// ListBoxのスクロール抑止
        /// </summary>
        /// <param name="e"></param>
            protected override void OnMouseWheel(MouseEventArgs e)
            {
                HandledMouseEventArgs wEventArgs = e as HandledMouseEventArgs;
                wEventArgs.Handled = true;


            try
            {
                int wheel = e.Delta / 120;
                int item数 = Items.Count;


                int CurrentItem = SelectedIndex;
                //例外を出さないようにする条件
                // CurrentItem - wheel がitem数以下に収まること
                if (SelectedIndex < item数)
                {

                    if (CurrentItem - wheel < item数 && CurrentItem - wheel > -1)
                    {
                        SetSelected(CurrentItem - wheel, true);
                    }



                }



            }

            catch (ArgumentOutOfRangeException)
            {// MessageBox.Show(OutEx.Message); }

                //例外は出ない
            }

        }
    }
}
