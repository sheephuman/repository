﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace Instant_Expenses
{
    public partial class Form1 
    {



        private void ReWriteFileButton_Click(object sender, EventArgs e)
        {
            dgv_WheelFix1.EndEdit();






            try
            {
                if (Combo_Displayed && comboBox1.Text != "")
                {
                    sfd.FileName = comboBox1.Text;



                    WritingFileMethod(current + "\\kakeibo\\" + sfd.FileName);
                    //"kakeibo"フォルダから読み込む


                    MessageBox.Show(sfd.FileName + "を保存しました");

                    Console.WriteLine(current + sfd.FileName);

                    CSV_to_chart(Path.GetFileName(sfd.FileName));
                }

                //起動時に読み込まれたままの場合
                else if (Properties.Settings.Default.LastLoadFile != "" && !Combo_Displayed)
                {
                    WritingFileMethod(Properties.Settings.Default.LastLoadFile);

                    MessageBox.Show(Properties.Settings.Default.LastLoadFile + "保存しました");

                    //     MessageBox.Show(LastLoad.Substring(LastLoad.IndexOf("kakeibo")));


                    CSV_to_chart(Path.GetFileName(LastLoad));
                }

                EditBool = false; //未編集フラグに戻す

                Text = Text.Replace("*", "");

                ReWriteStripBtn.Enabled = false;


            }


            catch (System.NullReferenceException)
            { MessageBox.Show("ファイルは開かれていません。既に開かれたファイルのみ上書き処理されます"); }
        }









       

        private void BottomAdd_CheckedChanged(object sender, EventArgs e)
        {
            if (BottomAdd.Checked == true)
            {
                BottomAdd.Text = "末尾に追加";

            }

            else if (BottomAdd.Checked == false)
            {
                BottomAdd.Text = "先頭に追加";
            }

        }


        bool NewData = false;     //新規データ判定
        private void 新規データ_Click(object sender, EventArgs e)
        {


            //編集済みか
            if (EditBool == true)
            {
                DialogResult result = MessageBox.Show("ファイルが保存されていません。保存しますか？",
       "質問",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2);




                //何が選択されたか調べる
                if (result == DialogResult.Yes)
                {
                    //新規データの場合
                    if (NewData == true)
                    {
                        //「はい」が選択された時
                        saveFileDig_Method();


                    }

                    EditBool = false; //未編集
                }

                else if (result == DialogResult.No)
                {
                    //「いいえ」が選択された時
                    goto label;
                }
            }

        label:
            NewData = true;

            ReWriteStripBtn.Enabled = false; //上書き保存無効化

            EditBool = false;  //編集状態

            dgv_WheelFix1.Rows.Clear();






            Text = "Instant Expencs " + " 新規データ";


            TotalCostCalc();



        }



        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            //if (comboBox1.Text != "")
            //    CSV_to_chart(comboBox1.Text);
        }


        private void Form1_Shown(object sender, EventArgs e)
        {

            if (YearChangeState.Checked == true)
            {
                CostBox.Focus();
            }
            else
            {
                DetailBox.Focus();
            }
        }




        private void 選択行の削除ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {

                if (hti != null)
                {
                    dgv_WheelFix1.Rows.RemoveAt(hti.RowIndex);
                }

            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("新規行は削除出来ません");


            }
            ReWriteStripBtn.Enabled = true;


            //未編集だった場合
            if (EditBool == false)
            {
                EditBool = true;

                Text += "*";

            }

            TotalCostCalc();
        }


        //アプリ終了時に設定を保存する処理
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.CommonColor = CommmonColor = chart1.Series[0].Color;
            Properties.Settings.Default.BottomAdd_CheckState = BottomAdd.Checked;
            Properties.Settings.Default.YearCangeChecked = YearChangeState.Checked;
            Properties.Settings.Default.Series_SUMCheckedState = Series_SUM_Chacker.Checked;
            Properties.Settings.Default.Cost_Cleared = Cost_ClearedState.Checked;
            Properties.Settings.Default.WindowsSize = Size;


           
            Properties.Settings.Default.spl2_Colasp = splitContainer2.Panel2Collapsed;
            Properties.Settings.Default.spl2_Fixed = splitContainer2.FixedPanel;


            if (ofd != null && ofd.SafeFileName != "")
            {
                LastLoad = ofd.FileName;

                Properties.Settings.Default.LastLoadFile = LastLoad;

            }
            else
            {
                Properties.Settings.Default.LastLoadFile = LastLoad;

            }
            Properties.Settings.Default.Save();


        }





        private void DetailBox_KeyDown(object sender, KeyEventArgs e)
        {



            int Select;


            if (DetailFocus == false && DetailBox.Text == "")
            {


                //DetailBox.Text = "";

                DetailFocus = true;


            }

            if (e.KeyCode == Keys.Tab)
            {
                CostBox.Focus();
            }

            if (e.KeyCode == Keys.Enter)
            {

                //フォーカス遷移
                CostBox.Focus();


                CostBox_KeyDown(sender, e);
                //CostBoxのKeyDownイベントも発動させる
                CostBox.Text = "";
            }

            else if (e.Shift == true && e.KeyCode == Keys.Down)
            {
                DetailBox.Text = "";
                DetailBox.DroppedDown = true;

            }

            else if (e.KeyCode == Keys.Delete && DetailBox.SelectedIndex > -1 && DetailBox.Items.Count > 1)
            {

                Select = DetailBox.SelectedIndex;

                DetailBox.Items.Remove(DetailBox.SelectedItem);




                if (DetailBox.SelectedIndex < DetailBox.Items.Count)
                {
                    if (DetailBox.SelectedIndex == -1)
                    {
                        DetailBox.SelectedIndex = 0;

                    }
                    if (Select > 0)
                    {

                        DetailBox.SelectedIndex = Select - 1;
                    }
                }

                DetailBox.Refresh();

                DetailBox.Text = "";
            }
        }

        private void CostBox_Enter(object sender, EventArgs e)
        {
            CostBox.ImeMode = ImeMode.Off;


        }






        private void reLoadToolStripMenuItem_Click(object sender, EventArgs e)
        {

            listBox_wheelfix1.Items.Clear();
            Itemリスト取得();
        }

        private void SortDotTextの編集ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process p = Process.Start("notepad.exe", current + "\\sort.txt");

        }

        private void ファイルリストのReload_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ファイルリスト取得();
        }

        bool TextFocus = false; //初回フォーカス判定

        private void DetailBOX_Enter(object sender, EventArgs e)
        {


            //初回フォーカス判定
            if (TextFocus == false)
            {


                TextFocus = true;

            }

            DetailBox.ImeMode = ImeMode.Hiragana;

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dateTimePicker1.ResetText();
            //今日の日付に戻す
        }





        bool Combo_Displayed = false; //ComboBoxから読み込まれたか
        private void dataViewに表示ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (EditBool == true)

            {
                NoFileSaveDialog();


            }


            ReWriteStripBtn.Enabled = false;
            EditBool = false; //未編集
            Combo_Displayed = true;

            NewData = false;
            string path = current + "\\kakeibo\\";

            CSV_to_DataGridView(path + comboBox1.Text);


            //最後に表示したファイルをcomboBoxのファイル名する
            LastLoad = path + comboBox1.Text;


            TotalCostCalc();
            sfd.FileName = comboBox1.Text;
         

            Text = comboBox1.Text;
        }


        /// <summary>
        /// ファイルが保存されていない場合の処理
        /// </summary>
        /// <param name="disposing"></param>
        #region ファイルが保存されていない場合の処理
        void NoFileSaveDialog()
        {

            //編集済みかどうか
            if (EditBool == true)
            {

                DialogResult result = MessageBox.Show("ファイルが保存されていません。保存しますか？",
                "質問",
              MessageBoxButtons.YesNo,
              MessageBoxIcon.Exclamation,
              MessageBoxDefaultButton.Button2);


                if (result == DialogResult.Yes)
                {

                    saveFileDig_Method();


                }

                else
                {
                    //「いいえ」が選択された時


                    goto jump;

                }

            }

        jump:

            EditBool = false; //未編集

            HistoyItms_Write("\\Details.txt");
            HistoyItms_Write("\\Costs.txt");

        }


        #endregion


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            NoFileSaveDialog();

        }




        private void reloadToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            DetailBox.Items.Clear();
            Historyリスト取得("\\Details.txt");
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Process p = Process.Start("notepad.exe", current + "\\Details.txt");


        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Process p = Process.Start("notepad.exe", current + "\\Costs.txt");

        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {

            CostBox.Items.Clear();
            Historyリスト取得("\\Costs.txt");
        }

        private void splitContainer2_SplitterMoved(object sender, SplitterEventArgs e)
        {
          //  label4.Text = splitContainer2.SplitterDistance.ToString();
        }




        private void splitContainer4_Panel1_Resize(object sender, EventArgs e)
        {
            // CancelEventArgs Begin = new CancelEventArgs();


            CancelEventArgs ce = new CancelEventArgs();


            if (splitContainer4.Panel1.Height < 90)
            {



                splitContainer4.SplitterDistance = 90;

            }


        }





    }

}


