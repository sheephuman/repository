﻿using System.Windows.Forms;

namespace WindowsFormsApp10
{
    public partial class DGW_WheelFix : DataGridView
    {
        public DGW_WheelFix()
        {
            InitializeComponent();
        }

        /// <summary>
        /// dataGridviewのスクロール抑止
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseWheel(MouseEventArgs e)
        {


            DoubleBuffered = true;
            //ちらつきを抑えパフォーマンスを向上させる


            int Wheel = e.Delta / 120;
            try
            {
                HandledMouseEventArgs wEventArgs = e as HandledMouseEventArgs;
                wEventArgs.Handled = true;


                //例外回避のための条件指定
                if (CurrentCell.RowIndex - Wheel < RowCount && CurrentCell.RowIndex - Wheel > -1)
                {


                    CurrentCell = Rows[CurrentCell.RowIndex - Wheel].Cells[CurrentCell.ColumnIndex];

                }
            }
            catch
            {  
                
                //例外は出ない }
            }
        }
    }
}